#!/usr/bin/env node

var express = require('express');
var app = express();
var port = Number(process.env.PORT || 5000);
//	Config
app.configure(function(){
	app.use(express.bodyParser());
	app.use(app.router);
	app.use(function(err, req, res, next){
		//	Throw error
		res.send(400, {
			"error": "Could not decode request: JSON parsing failed"
		});
	});
});
//	Get
app.get('/', function(req, res) {
	res.send('Make hay!');
});
//	Post
app.post('/', function(req, res){
	//	Get payload
	var _request = req.body.payload;
	var _response = [];
	//	Build response
	for(var i = 0; i < _request.length; i++){
		//	If DRM = true and more than one episode count
		if(_request[i].drm && _request[i].episodeCount > 0){
			//	Push to array
			_response.push({
				"image":_request[i].image.showImage,
				"slug":_request[i].slug,
				"title":_request[i].title
			});
		}
	}
	//	Parse back JSON
	res.json({"response":_response});
})
//	Listen
app.listen(port, function() {
	console.log('Http Server running at http://localhost:' + port + '/');
});